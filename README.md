# ggame_Project

## Building
To build this project on debian based distros <br />
1.) `sudo apt install cmake libsdl2-dev g++` <br />
2.) `cmake .`<br />
3.) `make`<br />

## Check in Proccess
`main` is the last stable release <br />
`release-x.x.x` are releases <br />
`edge` is the newest development release <br />
`dev-XXX` are feature branches that are waiting to be merged <br />

## Creating & Completing stories
We will be using the built in codeberg issues tab to deal with story creation. 
These stories are based around first come first serve. If you wish to create a
story simply mention in the XMPP chat and we can discuss story creation. If you are
a committer then simply create it as long as it is not a major design decision. Once
you have a story simply create a branch with the issue number included like DEV-xxx. 
Where the Xs are the number. When the story is ready to be merged open a pull request 
into edge. You should run the `lint.sh` over code that is committed so that we can have 
a universal style. This will require installing `clang-format` for debian distros this 
is `sudo apt install clang-format`

## Communication
The off board communication is an XMPP server at `theggame@conference.yourdata.forsale`. <br />
<ol>
<li>Download an XMPP client. If you are using debian the recommended client is dino. More clients <a target="_blank" rel="noopener noreferrer" href="https://xmpp.org/getting-started/">here</a></li>
<li>Create an account. Go into your client and add an account. It should have an option to create an account. Enter <code>yourdata.forsale</code> as the server address.</li>
<li>Enter a username and password. This is a completely new account so enter whatever</li>
<li>After creating the account go to join channel and enter <code>theggame@conference.yourdata.forsale</code></li>
</ol> 
